// Playing Cards
// Izaac Dewilde
//Megan Rycerz
//hello there

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	SPADES,
	DIAMONDS,
	HEARTS,
	CLUBS
};

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
}; 

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card c1;
	c1.rank = Rank::KING;
	c1.suit = Suit::CLUBS;

	(void)_getch();
	return 0;
}